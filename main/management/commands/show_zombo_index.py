from django.core.management.base import BaseCommand
from main.models import Article, ZomboIndex


class Command(BaseCommand):
    def handle(self, *args, **options):
        idx = ZomboIndex(url='elasticsearch:9200/', name='zombo_idx', fields=['text'])
        print(idx.__repr__())
